+++
title = "University of California San Diego"

[extra]
image = "ucsd-logo.jpg"
link = "https://ucsd.edu"
+++

BS Computer Engineering

Sep.2019 - Mar.2023
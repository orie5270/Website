+++
title = "About"
template = "about.html"

[extra]
author = "Carol Bao"
image = "IMG_9553.jpg"
+++

My name is Carol Bao, currently pursuing a Master's degree in Financial Engineering at Cornell University. With a profound passion for finance and data analytics, I immerse myself in the dynamic world where numbers meet strategy, striving to unravel complex financial puzzles. Beyond my academic pursuits, I find joy in building LEGO.